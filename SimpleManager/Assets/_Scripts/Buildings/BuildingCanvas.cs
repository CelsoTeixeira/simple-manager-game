using UnityEngine;
using UnityEngine.UI;

public class BuildingCanvas : MonoBehaviour
{
    public Image CircularImage;

    public void UpdateCircularImage(float currentWork, float totalWork)
    {
        CircularImage.fillAmount = currentWork / totalWork;
    }
}