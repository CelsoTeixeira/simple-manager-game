using System;

public enum BuildingStatus
{
    OnConstruction = 0,
    Working = 1,
}