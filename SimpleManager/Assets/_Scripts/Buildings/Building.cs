using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Building : MonoBehaviour
{
    [Header("Status")]
    public BuildingStatus Status;

    [Header("Information")]
    public BuildingConstructionInformation ConstructionInformation = new BuildingConstructionInformation();
    public BuildingWorkingInformation WorkingInformation = new BuildingWorkingInformation();

    [Header("Canvas")]
    public BuildingCanvas BuildindCanvas;

    private BuildingOnConstruction OnConstruction;
    private BuildingOnWorking OnWorking;

    public Action OnBuildPlotted;
    public Action OnBuildConstructed;
    
    private void Awake()
    {
        OnBuildPlotted += BuildStartConstruction;
        
        OnConstruction = new BuildingOnConstruction();
        OnConstruction.Information = ConstructionInformation;
        OnConstruction.OnConstructionComplete += ConstructionComplete;

        OnWorking = new BuildingOnWorking();
        OnWorking.Information = WorkingInformation;
        OnWorking.OnWorkingCycleComplete += OnWorkingCycle;
    }


    public void BuildingUpdate(float deltaTime)
    {
        Debug.Log("Updating");

        if (Status == BuildingStatus.OnConstruction)
        {
            OnConstruction.OnConstructionUpdate(deltaTime);
            BuildindCanvas.UpdateCircularImage(OnConstruction.InternalTimer, OnConstruction.Information.TimeToBuild);
        }
        else if (Status == BuildingStatus.Working)
        {
            OnWorking.OnWorkingUpdate(deltaTime);
            BuildindCanvas.UpdateCircularImage(OnWorking.InternalTimer, OnWorking.Information.TimeNecessary);
        }

    }

    public void BuildPloted()
    {
        if (OnBuildPlotted != null)
            OnBuildPlotted();

        //Inform that a new build plot has been added.
        BuildingController.Instance.InformBluidPlot(this);
    }

    private void BuildStartConstruction()
    {
        //Debug.Log("Start Construction");        
        Status = BuildingStatus.OnConstruction;
    }

    private void ConstructionComplete()
    {
        //Debug.Log("Construction Complete!");
        Status = BuildingStatus.Working;
    }

    private void OnWorkingCycle(ResourceType resource, float amount)
    {
        Debug.Log("Working Cycle Complete! R - " + resource + " / A - " + amount );

        InventoryController.Instance.AddResource(resource, (int)amount);
    }
}