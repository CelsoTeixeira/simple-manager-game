using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Building Library", menuName = "Manager/Building Library")]
public class BuildingLibrary : ScriptableObject
{
    [Header("Available to construct")]
    public List<BuildingData> Buildings;


    public BuildingData GetSelectedBuilding(int typeToGet)
    {
        return Buildings.Find(b => b.Type == (BuildingType)typeToGet);
    }
}