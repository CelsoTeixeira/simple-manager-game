using System;
using UnityEngine;

[Serializable]
public struct BuildingData
{
    public BuildingType Type;
    public Building Build;
}