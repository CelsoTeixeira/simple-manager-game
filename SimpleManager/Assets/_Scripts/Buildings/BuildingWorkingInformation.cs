using System;

[Serializable]
public struct BuildingWorkingInformation
{
    public ResourceType Resource;

    public float Amount;
    public float TimeNecessary;
}