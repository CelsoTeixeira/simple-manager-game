using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour
{
    public BuildingType TypeToConstruct;

    public Button Button;

    private void Awake()
    {
        Button = GetComponent<Button>();

        Button.onClick.AddListener(OnPress);
    }

    public void OnPress()
    {
        BuildingController.Instance.ChangeSelectedType((int)TypeToConstruct);
    }
}