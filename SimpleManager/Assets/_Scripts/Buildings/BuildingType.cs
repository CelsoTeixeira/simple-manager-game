public enum BuildingType 
{
    None = 0,
    House = 1,
    Farm = 2,
    LumberMill = 3,
    Castle = 4
    
}