using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingOnWorking
{
    public Action<ResourceType, float> OnWorkingCycleComplete;

    public BuildingWorkingInformation Information;

    public float InternalTimer { get; private set; }

    public void OnWorkingUpdate(float deltaTime)
    {
        //TODO(Celso): Before any working cycle start, we need to make sure we have the necessary amount of
        //resources to complete the job.

        InternalTimer += deltaTime;

        //Debug.Log("W - " + InternalTimer + " / " + Information.TimeNecessary);

        if (InternalTimer >= Information.TimeNecessary)
        {
            if (OnWorkingCycleComplete != null)
                OnWorkingCycleComplete(Information.Resource, Information.Amount);

            InternalTimer = 0;
        }
    }
}