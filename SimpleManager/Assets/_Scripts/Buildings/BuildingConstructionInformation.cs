using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct BuildingConstructionInformation
{
    [Header("Construction Cost")]
    public List<InventoryRawData> ConstructionCost;    

    [Header("Time to Build")]
    public float TimeToBuild;
}