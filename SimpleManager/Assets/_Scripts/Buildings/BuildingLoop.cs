using System.Collections.Generic;
using UnityEngine;

public class BuildingLoop
{
    public void Loop(List<Building> buildings)
    {
        for (int i = 0; i < buildings.Count; i++)
        {
            buildings[i].BuildingUpdate(Time.deltaTime);
        }
    }
}