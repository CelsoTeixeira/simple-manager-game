using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    #region Singleton

    private static BuildingController instance;

    public static BuildingController Instance 
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<BuildingController>();

            return instance;
        }
    }

    #endregion
    
    [Header("Library")]
    public BuildingLibrary BuildingLibrary;

    public List<Building> AllBuildings = new List<Building>();
    private BuildingLoop _buildingLoop = new BuildingLoop();

    public void Awake()
    {
        //Get the castle
        var allActiveObjects = GameObject.FindObjectsOfType<Building>().ToList();        
        AllBuildings.AddRange(allActiveObjects);
    }

    public void Update()
    {
        Debug.Log("Build Loop");
        _buildingLoop.Loop(AllBuildings);
    }

    public void InformBluidPlot(Building newBuild)
    {
        if (AllBuildings != null)
            AllBuildings = new List<Building>();

        AllBuildings.Add(newBuild);
    }

    /// <summary>
    /// The current building to be constructed by
    /// user input.
    /// </summary>
    public BuildingData ToConstruct { get; private set; }
    
    /// <summary>
    /// Called from the UI to inform
    /// that the user have clicked on a new 
    /// build to start construction.
    /// </summary>
    /// <param name="toChange">Int to be matched with the BuildingType enum</param>
    public void ChangeSelectedType(int toChange)
    {
        Debug.Log("Going to build: " + (BuildingType) toChange);

        //SelectedType = (BuildingType)toChange;
        ToConstruct = BuildingLibrary.GetSelectedBuilding(toChange);
    }
}