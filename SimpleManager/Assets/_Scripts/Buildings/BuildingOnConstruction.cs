using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingOnConstruction 
{
    public Action OnConstructionComplete;

    public BuildingConstructionInformation Information;

    public float InternalTimer { get; private set; }

    public void OnConstructionUpdate(float deltaTime)
    {   
        InternalTimer += deltaTime;

        // Debug.Log("C - " + internalTimer + " / " + Information.TimeToBuild);

        if (InternalTimer >= Information.TimeToBuild)
        {
            if (OnConstructionComplete != null)
                OnConstructionComplete();
        }
    }
}