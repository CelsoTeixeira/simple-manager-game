using UnityEngine;

public class MyTerrain : MonoBehaviour
{
    public Building Construction;

    public void Construct(Building toConstruct)
    {
        if (Construction != null)
        {
            Debug.Log("Tile Already Construct!");
            return;
        }

        Building newBuilding = Instantiate<Building>(toConstruct,
            this.transform.localPosition, Quaternion.identity, this.transform);
        newBuilding.transform.localEulerAngles = Vector3.zero;

        newBuilding.BuildPloted();
    }

    public void LogClick()
    {
        
    }

    public void OnMouseDown()
    {
        //Debug.Log("Clicked!");

        if (BuildingController.Instance.ToConstruct.Type != BuildingType.None)
        {
            Construct(BuildingController.Instance.ToConstruct.Build);
        }
    }
}