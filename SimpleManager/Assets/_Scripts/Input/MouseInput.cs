using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour
{
    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.Raycast();
        }
    }

    //Terrain
    private void Raycast()
    {
        return;

        var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        RaycastHit2D rayHit = Physics2D.Raycast(position, Vector2.zero);

        if (rayHit.collider != null)
        {
            if (rayHit.collider.CompareTag("Terrain"))
            {
                var myTerrain = rayHit.collider.GetComponent<MyTerrain>();

                if (BuildingController.Instance.ToConstruct.Type != BuildingType.None)
                {
                    myTerrain.Construct(BuildingController.Instance.ToConstruct.Build);
                }
            
                myTerrain.LogClick();
            }
        }
    }
}