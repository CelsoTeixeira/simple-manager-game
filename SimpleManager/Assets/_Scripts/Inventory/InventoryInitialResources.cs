﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryInitialResources", menuName = "Manager/Create Inventory Initial Resources configuration")]
public class InventoryInitialResources : ScriptableObject
{
    public List<InventoryRawData> InitialResource;
}