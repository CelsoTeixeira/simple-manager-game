﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    #region Singleton

    private static InventoryController _instance;

    public static InventoryController Instance
    {
        get
        {
            if (_instance == null) _instance = GameObject.FindObjectOfType<InventoryController>();
            return _instance;
        }
    }

    #endregion

    [Header("Raw Data")]
    public InventoryInitialResources InitialRawResources;
    public List<InventoryRawData> Inventory = new List<InventoryRawData>();

    [Header("UI Controller")]
    public UIInventoryController UIController = new UIInventoryController();

    public Action<ResourceType, int> OnResourceUpdate;

    public void Awake()
    {
        AttributeInitialResources();

        //Subscribe the UI Updater to every Resource change.
        OnResourceUpdate += UIController.UpdateComponent;

        //Do the first update on the initial data
        UIController.UpdateComponents(Inventory);
    }
    
    public void AddResource(ResourceType resource, int amount)
    {
        var newAmount = Inventory.Find(i => i.Resource == resource).AddAmount(amount);

        if (OnResourceUpdate != null)
            OnResourceUpdate(resource, newAmount);
    }

    public void RemoveResource(ResourceType resource, int amount)
    {
        var newAmount = Inventory.Find(i => i.Resource == resource).RemoveAmount(amount);

        if (OnResourceUpdate != null)
            OnResourceUpdate(resource, newAmount);
    }

    private void AttributeInitialResources()
    {
        Inventory = new List<InventoryRawData>();

        for (int i = 0; i < InitialRawResources.InitialResource.Count; i++)
        {
            Inventory.Add(new InventoryRawData()
            {
                Resource = InitialRawResources.InitialResource[i].Resource,
                Amount = InitialRawResources.InitialResource[i].Amount
            });
        }
    }

    
}
