﻿using System;

[Serializable]
public struct InventoryRawData
{
    public ResourceType Resource;
    public int Amount;

    public int AddAmount(int toAdd)
    {
        Amount += toAdd;
        return Amount;
    }

    public int RemoveAmount(int toRemove)
    {
        Amount -= toRemove;
        return Amount;
    }

    public bool HaveAmount(int amountToCheck)
    {
        var checkAmount = Amount - amountToCheck;
        return checkAmount >= 0;
    }
}