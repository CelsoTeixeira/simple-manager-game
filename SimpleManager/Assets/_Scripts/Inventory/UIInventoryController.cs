﻿using System;
using System.Collections.Generic;

[Serializable]
public class UIInventoryController
{
    public List<UIInventoryComponent> InventoryComponents = new List<UIInventoryComponent>();

    public void UpdateComponent(ResourceType resource, int amount)
    {
        InventoryComponents.Find(c => c.Resource == resource).UpdateText(amount);
    }

    public void UpdateComponents(List<InventoryRawData> rawData)
    {
        for (int i = 0; i < rawData.Count; i++)
        {
            UpdateComponent(rawData[i].Resource, rawData[i].Amount);
        }
    }
}