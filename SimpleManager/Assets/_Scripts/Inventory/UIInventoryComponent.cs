﻿using UnityEngine;
using UnityEngine.UI;

public class UIInventoryComponent : MonoBehaviour
{
    public ResourceType Resource;

    public Image ResourceIcon;
    public Text ResourceText;

    public void UpdateText(int newAmount)
    {
        ResourceText.text = newAmount.ToString();
    }
}