public enum ResourceType
{
    None = 0,
    Food = 1,    
    Wood = 2,
    People = 3
}