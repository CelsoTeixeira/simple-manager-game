using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPanel : MonoBehaviour
{
    public GameObject ToControl;

    public void Awake()
    {
        ToControl.SetActive(false);
    }

    public void OnPress()
    {
        ToControl.SetActive(!ToControl.activeInHierarchy);
    }
}